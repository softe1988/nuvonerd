let gulp        = require('gulp'),
  imagemin      = require('gulp-imagemin');

  var paths = {
    images : 'public/images-orig/*'
  }

  gulp.task('images', () =>
    gulp.src(paths.images)
      .pipe(imagemin())
      .pipe(gulp.dest('public/images'))
 );

 gulp.task('default', ['images']);