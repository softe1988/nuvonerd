if (process.env.NODE_ENV !== 'production') { require('dotenv').config() }
let express     = require('express'),
  cors          = require('cors'),
  path          = require('path'), 
  cookieParser  = require('cookie-parser'),
  logger        = require('morgan'),
  ejs           = require('ejs'),
  compression   = require('compression'),
  butter        = require('buttercms')(process.env.BUTTER_API_KEY);

const app = express();

app.set('view engine', 'ejs');
app.set('views', [__dirname + '/views/pages/',__dirname + '/views/partials/']);

app.use(compression());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', renderHomepage);
app.get('/blog', renderBlogHome); 
app.get('/blog/:slug', renderPost);
app.get('/resume', function(req, res) {
  let file = __dirname + '/public/resume_2018.pdf';
  res.download(file)
});

function renderHomepage(req, res) {
  res.render('index');
}

function renderBlogHome(req, res) {
  var page = req.params.page || 1;

  butter.post.list({page: page, page_size: 25})
  .then(function(blogres) {

    res.render('bloghome', {
      posts: blogres.data.data,
      next_page: blogres.data.meta.next_page,
      previous_page: blogres.data.meta.previous_page
    })
  })
  .catch((err) => {
    console.log(err.message);
  })
}

function renderPost(req, res) {
  var slug = req.params.slug;

  butter.post.retrieve(slug).then(function(resp) {
    res.render('post', {
      title: resp.data.data.title,
      post: resp.data.data,
      published: new Date(resp.data.data.published)
    
      })
    })
  .catch((err) => {
      console.log(err.message);
  })
}

app.use(function (req, res, next) {
  res.status(404).render("notfound")
})

module.exports = app;
